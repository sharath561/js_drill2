//const inventory = require('./inventory')
// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function bmwandaudi(data){

    const gotdata = data.filter(car => car.car_make == "BMW" || car.car_make == "Audi").map(car => JSON.stringify(car))
    return gotdata
}

//console.log(bmwandaudi(inventory))

module.exports = bmwandaudi;