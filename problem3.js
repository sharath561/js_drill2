//const inventory = require('./inventory')
// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortmodels(data){

    return data.sort((a,b) => (a.car_model > b.car_model ? 1 : -1)).map(car => car.car_model)
}

//console.log(sortmodels(inventory))

module.exports = sortmodels;